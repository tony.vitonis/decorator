# Python 3 Function Decorators

This project provides a simple Python 3 base class to encapsulate
boilerplate code for function decorators.

## decorator.py

Provides the Decorator base class. The class does nothing on its
own. Usage documentation is provided as a class docstring. The
docstring also contains doctest code that will run if the module
is invoked directly from the command line.
