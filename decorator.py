class Decorator:
   """ A generic Python 3 base class for function decorators.

   Users should inherit from this class and provide a "result" method, which:

      a) will have the decorator args available in self.args,
      b) will have the decorator kwargs available in self.kwargs,
      c) will have the decorated function available in self.function,
      d) will be passed args and kwargs from the decorated function call, and
      e) must return the result of self.function(*args, **kwargs).

   Derived decorators must be the last in a decorator chain. A usage example:

   >>> class say_hi_before(Decorator):
   ...    def result(self, *args, **kwargs):
   ...       print("hi from", self.function.__name__, self.args, self.kwargs)
   ...       return self.function(*args, **kwargs)
   ...
   >>> def maincall(): print(standalone_function(A().instance_method))
   ...
   >>> @say_hi_before("arg1", "arg2", kwarg="mykwarg")
   ... def standalone_function(arg): return "bye " + arg
   ...
   >>> class A:
   ...    @property
   ...    @say_hi_before
   ...    def instance_method(self): return "alfred"
   ...
   >>> maincall()
   hi from instance_method () {}
   hi from standalone_function ('arg1', 'arg2') {'kwarg': 'mykwarg'}
   bye alfred
   """

   def __init__(self, *args, **kwargs):

      self.invoked_with_parentheses = not self._arg_is_function(args)

      if self.invoked_with_parentheses:
         self.args, self.kwargs = args, kwargs
      else:
         self.function, self.args, self.kwargs = args[0], (), {}

   def _arg_is_function(self, args):

      import types
      return len(args) == 1 and isinstance(args[0], types.FunctionType)

   def __call__(self, *args, **kwargs):

      if self.invoked_with_parentheses:
         return self._wrapped_function(*args, **kwargs)
      else:
         return self._call_result(*args, **kwargs)

   def _wrapped_function(self, *args, **kwargs):

      self.function = args[0]

      def wrapper(*args, **kwargs):
         return self._call_result(*args, **kwargs)

      return wrapper

   def _call_result(self, *args, **kwargs):

      try:
         return self.result(*args, **kwargs)
      except AttributeError:  # No self.result in child object
         return self.function(*args, **kwargs)

   def __get__(self, obj, type=None):

      import functools
      return functools.partial(self.__call__, obj)

if __name__ == "__main__":
   import doctest
   doctest.testmod()

"""
Decorators can be invoked in one of two ways:

   1) Without parentheses:

      @mydecorator
      def some_function(...): ...

   2) With [possibly empty] parentheses:

      @mydecorator(...)
      def some_function(...): ...

In case 1:

   a) __init__ is called when the decorator is processed. Its only argument
      is the decorated function. The decorator object is thereafter used
      instead of the original function.
   b) __call__ is called when the decorated function is invoked. Its arguments
      are those passed to the function.

In case 2:

   a) __init__ is called when the decorator is processed. Its arguments are
      those passed to the decorator.
   b) __call__ is called immediately after __init__. Its only argument is the
      decorated function. It returns the wrapped function. The wrapped function
      is thereafter used instead of the original function.

Note: I know that it feels a little clumsy to talk about invoking the decorator
      "with or without parentheses" instead of "with or without arguments".
      However, when you invoke the decorator like this ...

      @mydecorator()

      ... the parentheses are there, but the arguments are not. With this in
      mind, I've made my comments clumsy but accurate.

TODO: Explain the reason for and functioning of the __get__ method.
"""
